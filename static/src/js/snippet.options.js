odoo.define('theme_leap.snippets.options', function (require) {
'use strict';

require('web.dom_ready');
var core = require('web.core');
var options = require('web_editor.snippets.options');

options.registry.sizing_negative_y = options.registry.sizing.extend({

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @override
     */
    _getSize: function () {

        var nClass = 'zt';
        var nProp = '';
        var sClass = 'zb';
        var sProp = '';

        var grid = [];

        for (var i = 1 ; i <= 6 ; i++) {
            for (var j = 1 ; j <= 20 ; j++) {
                grid.push(i);
            }
        }

        this.grid = {
            n: [_.map(grid, function (v) { return nClass + v; }), grid, nProp],
            s: [_.map(grid, function (v) { return sClass + v; }), grid, sProp],
        };
        return this.grid;
    },

});

options.registry.theme_sizing_y = options.registry.sizing.extend({

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @override
     */
    _getSize: function () {

        var nClass = 'pt-';
        var nProp = 'padding-top';
        var sClass = 'pb-';
        var sProp = 'padding-bottom';

        var grid = [];

        var rem = 16;
        var spacer = 1.5 * rem;
        var spacing = [0, spacer*0.25, spacer*0.5, spacer, spacer*1.5, spacer*2, spacer*3, spacer*4]

        var i = 0;
        $.each(spacing, function( index, value ) {
            for (var j = i; j <= value; j++) {
                grid.push(index);
                i++;
            }
        });

        this.grid = {
            n: [_.map(grid, function (v) { return nClass + v; }), grid, nProp],
            s: [_.map(grid, function (v) { return sClass + v; }), grid, sProp],
        };
        return this.grid;
    },

});

/*

moved to website_divider

options.registry.has_divider = options.Class.extend({

    */
/**
     * @override
     *//*

    toggleClass: function () {
        this._super.apply(this, arguments);
        this.$target.find($('[class*=divider'))
            .toggleClass('d-none',
            !this.$target.hasClass('has-divider'));
    },

    */
/**
     * @override
     *//*

    cleanForSave: function () {
        if (!this.$target.hasClass('has-divider')) {
            var divider = this.$target.find($('[class*=divider'));
            divider.length && divider.remove();
        }
    },

});

options.registry.set_divider = options.Class.extend({

    sprite: '/theme_leap/static/src/img/sprite.svg',

    */
/**
     * @override
     *//*

    start: function () {
        this._super.apply(this, arguments);
        var self = this;

        this.$target.on('snippet-option-change', function () {
            self.onFocus();
        });
    },

    */
/**
     * @override
     *//*

    onFocus: function () {
        this.$el.toggleClass('d-none', !this.$target.hasClass('has-divider'));
    },

    //--------------------------------------------------------------------------
    // Options
    //--------------------------------------------------------------------------

    top: function (previewMode, value) {
        if (previewMode !== false) {
            return;
        }

        var divider = this.$target.find('.divider-top');
        divider.length && divider.remove();

        if (value && value !== 'none') {
            var divider = $('<div/>', {class: 'divider-top'})
                .append('<svg class="bg-white"><use href="' + this.sprite + '#' + value + '"/></svg>')
            this.$target.prepend(divider);
        }

        this._setActive();
    },

    bottom: function (previewMode, value) {
        if (previewMode !== false) {
            return;
        }

        var divider = this.$target.find('.divider-bottom');
        divider.length && divider.remove();

        if (value && value !== 'none') {
            var divider = $('<div/>', {class: 'divider-bottom'})
                .append('<svg class="bg-white"><use href="' + this.sprite + '#' + value + '"/></svg>')
            this.$target.append(divider);
        }

        this._setActive();
    },

    flip: function (previewMode, value) {
        if (previewMode !== false) {
            return;
        }

        var divider = this.$target.find('.divider-' + value);
        divider.length && divider.toggleClass('flip-x');
    },

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    */
/**
     * @private
     * @override
     *//*

    _setActive: function () {
        var self = this;
        this._super.apply(this, arguments);

        $.each(['top', 'bottom'], function(index, value) {
            var href = self.$target.find('.divider-' + value + ' use').attr('href');
            var divider = href && href.split("#").pop() || 'none';
            self.$el
                .find('[data-' + value + ']').removeClass('active')
                .filter('[data-' + value + '="' + divider + '"]').addClass('active');
        });
    },

});
*/


});


/*
moved to website_svg_symbol

odoo.define('theme_leap.wysiwyg', function (require) {

var core = require('web.core');
var media = require('wysiwyg.widgets.media');
var MediaDialog = require('wysiwyg.widgets.MediaDialog');

var _t = core._t;

var SvgSymbolWidget = media.SearchableMediaWidget.extend({
    template: 'wysiwyg.widgets.svg-symbols',

    events: _.extend({}, media.SearchableMediaWidget.prototype.events || {}, {
        'click .svg-symbols-symbol': '_onSymbolClick',
        'dblclick .svg-symbols-symbol': '_onSymbolDblClick',
    }),

    */
/**
     * @constructor
     *//*

    init: function (parent, media) {
        this._super.apply(this, arguments);
        var self = this;

        this.symbolsParser = [];

*/
/*
        this._parseSymbols2().then(function(result) {
            console.log("Success!", result);
            this.symbolsParser = result;
        });
*//*

*/
/*        this._parseSymbols().then(function(result) {
            console.log("Success!", result);
            this.symbolsParser = result;
        });*//*


        this.symbolsParser = this._parseSymbols4();

        console.log('init symbols', this.symbolsParser);

*/
/*
        (async () => {

            console.log('async start');

            let response = await fetch('/theme_leap/static/src/img/sprite.svg');
            let xml = await response.text();
            xmlDoc = $.parseXML(xml);

            var symbolsParser = [];

            var symbols = xmlDoc.getElementsByTagName("symbol");
            for (i=0; i < symbols.length; i++) {
                if (symbols[i].id.match('icon')) {
                    var title = symbols[i].getElementsByTagName('title')[0];
                    var data = {id: symbols[i].id, name: title && title.textContent,};
                    symbolsParser.push(data);
                }
            }

            console.log('async symbols', symbolsParser);

        })();


        console.log('init symbols', this.symbolsParser);
        *//*


    },


    */
/**
     * @override
     *//*

    start: function () {
        //def = this._super.apply(this, arguments);

        console.log('start symbols', this.symbolsParser);

        //return def;
    },


    */
/**
     * @override
     *//*

    save: function () {

*/
/*
        var $span = $('<span/>');
        console.log('media', this.$media, this.media);
        $span.data(this.$media.data());
        this.$media = $span;
        this.media = this.$media[0];

        //this.$media.append('<h1>got ya</h1>');
        this.$media.append(this.selectedIcon);
*//*


        console.log(this.selectedIcon.find('use'), this.selectedIcon.find('use').attr('href'));

        var $symbol = $('<svg><use href="'+ this.selectedIcon.find('use').attr('href') +'"></use></svg>');
        $symbol.data(this.$media.data());

        this.$media = $symbol;
        this.media = this.$media[0];

        return Promise.resolve(this.media);

    },

    */
/**
     * @override
     *//*

    search: function (needle) {
        console.log('search');
    },

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    */
/**
     * @private
     *//*

    _parseSymbols: function() {
        var symbolsParser = [];
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/theme_leap/static/src/img/sprite.svg', false);
        xhr.send();
        xhr.onload = function() {
            var xmlDoc = xhr.responseXML;
            var symbols = xmlDoc.getElementsByTagName("symbol");
            for (i=0; i < symbols.length; i++) {
                if (symbols[i].id.match('icon')) {
                    var title = symbols[i].getElementsByTagName('title')[0];
                    var data = {id: symbols[i].id, name: title && title.textContent,};
                    symbolsParser.push(data);
                }
            }
*/
/*
            if (xhr.status != 200) { // analyze HTTP status of the response
                alert(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
            } else { // show the result
                alert(`Done, got ${xhr.response.length} bytes`); // responseText is the server
            }
*//*

        };

        console.log('_parseSymbols', symbolsParser);

        //symbolsParser = [{id: 'icon-1',},{id: 'icon-2',}];

        //symbolsParser = [];

        return symbolsParser;
    },

    _parseSymbols2: function() {

        var promise = new Promise(function (resolve) {

            var symbolsParser = [];
            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/theme_leap/static/src/img/sprite.svg');

            xhr.onload = function() {
                var xmlDoc = xhr.responseXML;
                var symbols = xmlDoc.getElementsByTagName("symbol");
                for (i=0; i < symbols.length; i++) {
                    if (symbols[i].id.match('icon')) {
                        var title = symbols[i].getElementsByTagName('title')[0];
                        var data = {id: symbols[i].id, name: title && title.textContent,};
                        symbolsParser.push(data);
                    }
                }
    */
/*
                if (xhr.status != 200) { // analyze HTTP status of the response
                    alert(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
                } else { // show the result
                    alert(`Done, got ${xhr.response.length} bytes`); // responseText is the server
                }
    *//*

            };
            xhr.send();

            resolve(symbolsParser);
        });

        return promise;

        var bimbim;

        var bla = function (result) {

            console.log('result', result);

            bimbim = result;

            return result;
        };

        var a = promise.then(bla);

        console.log('a', a);


        return a;



        //return symbolsParser;
    },



    _parseSymbols3: function() {


        return new Promise(function (resolve) {

            var symbolsParser = [];
            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/theme_leap/static/src/img/sprite.svg');
            xhr.send();
            xhr.onload = function() {
                var xmlDoc = xhr.responseXML;
                var symbols = xmlDoc.getElementsByTagName("symbol");
                for (i=0; i < symbols.length; i++) {
                    if (symbols[i].id.match('icon')) {
                        var title = symbols[i].getElementsByTagName('title')[0];
                        var data = {id: symbols[i].id, name: title && title.textContent,};
                        symbolsParser.push(data);
                    }
                }

                resolve(symbolsParser);
    */
/*
                if (xhr.status != 200) { // analyze HTTP status of the response
                    alert(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
                } else { // show the result
                    alert(`Done, got ${xhr.response.length} bytes`); // responseText is the server
                }
    *//*

            };
        });


        //return promise;
    },



    _parseSymbols4: function() {


        function fetchSvg() {
          return fetch('/theme_leap/static/src/img/sprite.svg')
          .then(
            response => response.text(),
            error => error)
          .then(function(text) {
            return text;
          });
        };


        var symbolsParser = [];

        (async () => {

            console.log('async start');

            let xml = await fetchSvg();
            var xmlDoc = $.parseXML(xml);

            var symbols = xmlDoc.getElementsByTagName("symbol");
            for (i=0; i < symbols.length; i++) {
                if (symbols[i].id.match('icon')) {
                    var title = symbols[i].getElementsByTagName('title')[0];
                    var data = {id: symbols[i].id, name: title && title.textContent,};
                    symbolsParser.push(data);
                }
            }

            console.log('async symbols', symbolsParser);

        })();

        return symbolsParser;
    },


    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------

    */
/**
     * @private
     *//*

    _onSymbolClick: function (ev) {
        ev.preventDefault();
        ev.stopPropagation();

//        this.selectedIcon = $(ev.currentTarget).data('id');
//        this._highlightSelectedIcon();

        this.selectedIcon = $(ev.currentTarget);

        console.log('click', ev.currentTarget);

    },

    */
/**
     * @private
     *//*

    _onSymbolDblClick: function () {
        this.trigger_up('save_request');
    },

});

MediaDialog.include({

    xmlDependencies: MediaDialog.prototype.xmlDependencies.concat(
        ['/theme_leap/static/src/xml/wysiwyg.xml']
    ),

    events: _.extend({}, MediaDialog.prototype.events, {
        'click #editor-media-svg-symbol-tab': '_onClickThemeIconTab',
    }),

    init: function (parent, options, media) {
        this._super.apply(this, arguments);

        this.svgSymbolWidget = new SvgSymbolWidget(this, media, options);
    },

    start: function () {
        var promises = [this._super.apply(this, arguments)];

        if (this.svgSymbolWidget) {
            promises.push(this.svgSymbolWidget.appendTo(this.$("#editor-media-svg-symbol")));
        }

        return Promise.all(promises);
    },

    //--------------------------------------------------------------------------
    // Public
    //--------------------------------------------------------------------------

    */
/**
     * Returns whether the theme icon widget is currently active.
     *
     * @returns {boolean}
     *//*

    isThemeIconActive: function () {
        return this.activeWidget === this.svgSymbolWidget;
    },

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    */
/**
     * Call clear on all the widgets except the activeWidget.
     *
     * @private
     *//*


    _clearWidgets: function () {
        this._super.apply(this, arguments);

        if (this.svgSymbolWidget !== this.activeWidget) {
            this.svgSymbolWidget && this.svgSymbolWidget.clear();
        }
    },

    */
/**
     * Sets the theme icon widget as the active widget.
     *
     * @private
     *//*

    _onClickThemeIconTab: function () {
        this.activeWidget = this.svgSymbolWidget;
    },

});

});
*/
