// TODO probably move to assets-editor?

odoo.define('theme_leap.rte.summernote', function (require) {
'use strict';

var core = require('web.core');
require('web_editor.rte.summernote');

var dom = $.summernote.core.dom;
var renderer = $.summernote.renderer;

/*
// Ignore svg images

moved to website_svg_symbol

var fn_is_img = dom.isImg || function () {};
dom.isImg = function (node) {
    var nodeName = node && node.nodeName.toUpperCase();
    if (node && (nodeName === "SVG" || nodeName === "USE" || nodeName === "PATH")) {
        return false;
    }
    var ret = fn_is_img(node);
    return fn_is_img(node);
};
*/


// TODO
var fn_createPalette = renderer.createPalette;
renderer.createPalette = function ($container, options) {
    fn_createPalette.call(this, $container, options);

    //alert('pim');

    // console.log(this);
    // console.log($container);
    // console.log(options);

};


});



odoo.define('theme_leap.content.menu', function (require) {
'use strict';

var publicWidget = require('web.public.widget');

// TODO think how to fix it
publicWidget.registry.affixMenu2 = publicWidget.registry.affixMenu.extend({

    start: function () {
        this._super.apply(this, arguments);
        _.each(this.$headers, function (el) {
            var $header = $(el);
            var $collapse = $header.find('.collapse');

            $collapse.on('show.bs.collapse', function () {
                $header.addClass('navbar-toggled-show');
            })
            $collapse.on('hidden.bs.collapse', function () {
                $header.removeClass('navbar-toggled-show');
            })
        });
    },

});

});
