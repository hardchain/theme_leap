from odoo import api, fields, models, tools


class Website(models.Model):
    _inherit = 'website'

    email = fields.Char()
    phone = fields.Char()
