from odoo import models


class ThemeLeap(models.AbstractModel):
    _inherit = 'theme.utils'

    def _theme_leap_post_copy(self, mod):
        self.disable_view('website_theme_install.customize_modal')
