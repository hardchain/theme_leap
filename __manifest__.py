{
    'name': 'Theme Leap',
    'category': 'Theme',
    'sequence': 1000,
    'version': '1.0',
    'depends': [
        'portal',
        'website',
        'web_editor',
        'website_theme_install',
        'website_svg_symbol'
    ],
    'data': [
        'data/data.xml',
        'views/assets.xml',
        'views/portal.xml',
        'views/website.xml',
        'views/snippets.xml',
    ],
    'images': [
    ],
    'application': False,
}
